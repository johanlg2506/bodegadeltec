from django.db import models
#

from django.contrib.auth.models import BaseUserManager


class ResourcesManager(BaseUserManager, models.Manager):

    def registrar_recurso( self, categoria, codigo, nombre, descripcion,  id_user_asignado=None,**extra_fields):
        recurso = self.model(
            categoria = categoria,
            codigo = codigo,
            nombre = nombre,
            descripcion = descripcion,
            id_user_asignado = id_user_asignado,
            **extra_fields
        )
        
        recurso.save(using=self.db)

        return recurso

    def filtrar_recursos_x_codigo(self, kword):
        resultado = self.filter(codigo__icontains=kword)              
        return resultado

    def mostrar_todos(self):
        resultado = self.all()
        return resultado

    def consultar_detalles(self,pk):
        resultado = self.filter(id=pk)
        return resultado

    def consultar_recursos_asignados(self,id_user):
        resultado = self.filter(id_user_asignado=id_user)
        return resultado

    
