from django.db import models
# Create your models here.

from applications.users.models import User

from .managers  import ResourcesManager

class Categoria(models.Model):
    nombre = models.CharField(max_length=150)

    def __str__(self):
        return self.nombre


class Resource(models.Model):
    categoria           = models.ForeignKey(Categoria,on_delete=models.CASCADE)
    codigo              = models.CharField(max_length=15)
    nombre              = models.CharField(max_length=50)
    descripcion         = models.CharField(max_length=250)
    id_user_asignado    = models.ForeignKey(User, on_delete=models.CASCADE, null=True)

    objects = ResourcesManager()
