from django.views.generic import ListView
from django.urls import reverse_lazy, reverse
from django.http import HttpResponseRedirect
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import DetailView, UpdateView

from django.views.generic.edit import FormView

from .models    import Resource
from .forms     import ResourceRegisterForm


# Create your views here.

class ResourceRegisterView(LoginRequiredMixin,FormView):
    template_name = 'resources/register.html'
    form_class = ResourceRegisterForm
    success_url = reverse_lazy('home_app:home-page')
    login_url = reverse_lazy('users_app:user-login')


    def form_valid(self, form):
        Resource.objects.registrar_recurso(
            form.cleaned_data['categoria'],
            form.cleaned_data['codigo'],
            form.cleaned_data['nombre'],
            form.cleaned_data['descripcion'],            

        )
        return super(ResourceRegisterView, self).form_valid(form)


class ResourcesListView(ListView):
    model = Resource
    template_name = "resources/administrar-recursos.html"
    context_object_name = 'recursos'
    paginate_by = 8


    def get_queryset(self):

        palabra_clave = None
        palabra_clave = self.request.GET.get("kword",'')
        
        if(palabra_clave!=None):        
            lista_recursos = Resource.objects.filtrar_recursos_x_codigo(palabra_clave)
        
        if(palabra_clave==''):
            lista_recursos = Resource.objects.mostrar_todos()
        
        
        return lista_recursos


class DetalleAsignacionRecursoView(LoginRequiredMixin,DetailView):
    models = Resource
    template_name = 'resources/detalle-asignacion.html'
    login_url = reverse_lazy('users_app:user-login')
    context_object_name = 'objeto'

    def get_queryset(self):
        
        pk = self.kwargs['pk']
        datos_detalle = Resource.objects.consultar_detalles(pk)
        return datos_detalle


class AsignarRecursoView(UpdateView):
    model = Resource
    template_name = 'resources/asignar-recurso.html'
    context_object_name = 'objeto'
    success_url = reverse_lazy('resources_app:administrar-recursos')
    fields=[
        'id_user_asignado'
    ]

    print(fields)


def quitar_asignacion_recurso(request,pk):
    Resource.objects.filter(id=pk).update(id_user_asignado=None)
    return HttpResponseRedirect(
            reverse(
                'resources_app:administrar-recursos'
            )
        )


