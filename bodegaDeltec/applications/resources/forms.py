from django import forms
from .models import Resource

class ResourceRegisterForm(forms.ModelForm):

    class Meta:
        model = Resource
        fields = (
            'categoria',
            'codigo',
            'nombre',
            'descripcion')

        widgets = {
            'codigo':forms.TextInput(
                attrs={
                    'type':'text',
                    'class':'form-control',
                    'placeholder':'Codigo Recurso',
                    'name':'codigo',
                    'arial-label':'Codigo Recurso'

                }
            ),
            'nombre':forms.TextInput(
                attrs={
                    'type':'text',
                    'class':'form-control',
                    'placeholder':'Nombre Recurso',
                    'name':'nombre',
                    'arial-label':'Nombre Recurso'

                }
            ),    
            'descripcion':forms.Textarea(
                attrs={
                    'type':'text',
                    'class':'form-control',
                    'placeholder':'Descripcion Recurso',
                    'name':'descripcion',
                    'arial-label':'Descripcion Recurso'

                }
            ),            
        }
