from django.urls import path

from . import views
from applications.resources.views import quitar_asignacion_recurso

app_name = "resources_app"

urlpatterns = [
    path(
        'registrar-recurso/', 
        views.ResourceRegisterView.as_view(),
        name='registrar-recurso',
    ),
    path(
        'administrar-recursos/', 
        views.ResourcesListView.as_view(),
        name='administrar-recursos',
    ),
    path(
        'detalle-asignacion-recurso/<pk>', 
        views.DetalleAsignacionRecursoView.as_view(),
        name='detalle-asignacion-recurso',
    ),
    path(
        'quitar-asignacion-recurso/<pk>', 
        quitar_asignacion_recurso,
        name='quitar-asignacion-recurso',
    ),
    path(
        'asignar-recurso/<pk>', 
        views.AsignarRecursoView.as_view(),
        name='asignar-recurso',
    ),

]

