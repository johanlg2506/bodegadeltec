from typing import ClassVar
from django.shortcuts import render
from django.urls import reverse_lazy
# Create your views here.

from django.contrib.auth.mixins import LoginRequiredMixin

from django.views.generic import TemplateView


class HomePageView(LoginRequiredMixin,TemplateView):
    template_name = "home/index.html"
    login_url = reverse_lazy('users_app:user-login')
