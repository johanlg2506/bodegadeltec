from django.db import models

# Create your models here.

from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin

#

from .functions import validate_id_user, validate_name, validate_last_name
from .managers  import UserManager

class User(AbstractBaseUser, PermissionsMixin):
    id_user     = models.CharField(max_length=20,unique=True,validators=[validate_id_user])
    name        = models.CharField(max_length=50,validators=[validate_name])
    last_name   = models.CharField(max_length=50,validators=[validate_last_name])
    is_staff    = models.BooleanField(default=False)

    USERNAME_FIELD  = 'id_user'
    REQUIRED_FIELDS = ['name', 'last_name']

    objects = UserManager()

    def get_username(self):
        return self.name

    def get_full_name(self):
        return self.name + ' ' + self.last_name
