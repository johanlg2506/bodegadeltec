from django.urls import path

from . import views

app_name = "users_app"

urlpatterns = [
    path(
        'register/', 
        views.UserRegisterView.as_view(),
        name='user-register',
    ),
    path(
        'login/', 
        views.UserLoginView.as_view(),
        name='user-login',
    ),
    path(
        'logout/', 
        views.UserLogoutView.as_view(),
        name='user-logout',
    ),
    path(
        'administrar-usuarios/', 
        views.UserListView.as_view(),
        name='administrar-usuarios',
    ),
    path(
        'ver-recursos-asignados/<id_user>', 
        views.UserAsignacionesListView.as_view(),
        name='ver-recursos-asignados',
    ),
]

