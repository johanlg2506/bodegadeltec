# Create your views here.
from django.urls import reverse_lazy, reverse
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect
from django.contrib.auth.mixins import LoginRequiredMixin

from django.views.generic import View, ListView
from django.views.generic.edit import FormView


from .forms import UserRegisterForm, UserLoginForm

from .models import User

from applications.resources.models import Resource


class UserRegisterView(LoginRequiredMixin,FormView):
    template_name = 'users/register.html'
    form_class = UserRegisterForm
    success_url = reverse_lazy('home_app:home-page')
    login_url = reverse_lazy('users_app:user-login')


    def form_valid(self, form):
        
        User.objects.create_user(
            form.cleaned_data['id_user'],
            form.cleaned_data['name'],
            form.cleaned_data['last_name']
        )

        return super(UserRegisterView, self).form_valid(form)


class UserLoginView(FormView):
    template_name = 'users/login.html'
    form_class = UserLoginForm
    success_url = reverse_lazy('home_app:home-page')


    def form_valid(self, form):
        user = authenticate(
            id_user  = form.cleaned_data['id_user'],
            password = form.cleaned_data['password']
        )

        login(self.request, user)
    
        return super(UserLoginView, self).form_valid(form)


class UserLogoutView(LoginRequiredMixin,View):
    login_url = reverse_lazy('users_app:user-login')

    def get(self, request):
        logout(request)
        return HttpResponseRedirect(
            reverse(
                'users_app:user-login'
            )
        )


class UserListView(ListView):
    model = User
    template_name = "users/administrar-usuarios.html"
    context_object_name = 'usuarios'

    def get_queryset(self):

        palabra_clave = self.request.GET.get("kword",'')

        lista_usuarios = User.objects.filtrar_usuarios(palabra_clave)
        
        return lista_usuarios

class UserAsignacionesListView(ListView):
    model = User
    template_name = "users/ver-recursos-asignados.html"
    context_object_name = 'recursos'

    def get_queryset(self):
        
        id_user = self.kwargs['id_user']
        datos_detalle = Resource.objects.consultar_recursos_asignados(id_user)
        return datos_detalle

     