from django import forms
from .models import User
from django.contrib.auth import authenticate

class UserRegisterForm(forms.ModelForm):

    class Meta:
        model = User
        fields = (
            'id_user',
            'name',
            'last_name')

        widgets = {
            'id_user':forms.TextInput(
                attrs={
                    'type':'text',
                    'class':'form-control',
                    'placeholder':'Numero Identificacion',
                    'name':'id_user',
                    'arial-label':'Numero Identificacion'

                }
            ),
            'name':forms.TextInput(
                attrs={
                    'type':'text',
                    'class':'form-control',
                    'placeholder':'Nombres',
                    'name':'name',
                    'arial-label':'Nombres'

                }
            ),
            'last_name':forms.TextInput(
                attrs={
                    'type':'text',
                    'class':'form-control',
                    'placeholder':'Apellidos',
                    'name':'last_name',
                    'arial-label':'Apellidos'

                }
            ),            
        }

    def clean_id_user(self):
        id_user = self.cleaned_data['id_user']
        id_user_exist = User.objects.filter(id_user=id_user).exists()
        if id_user_exist:
            raise forms.ValidationError('El numero de identificacion ya existe')
        return id_user


class UserLoginForm(forms.Form):
    
    id_user=forms.CharField(
        label='Numero Identificacion',
        required=True,
        widget=forms.TextInput(
            attrs={
                'type':'text',
                'class':'form-control',
                'placeholder':'Numero Identificacion',
                'name':'id_user',
                'arial-label':'Numero Identificacion'
            }
        )
    )

    
    password=forms.CharField(
        label='Contraseña',
        required=True,
        widget=forms.PasswordInput(
            attrs={
                'type':'password',
                'class':'form-control',
                'placeholder':'Contraseña',
                'name':'password'
                
            }
        )
    )

    def clean(self):
        id_user = self.cleaned_data['id_user']
        password = self.cleaned_data['password']        
        id_user_exist = User.objects.filter(id_user=id_user).exists()
        if not id_user_exist:
            self.add_error('id_user','El numero de identificacion no se encuentra registrado')
        else:
            if id_user and password:
                user = authenticate(
                    id_user  = id_user,
                    password = password       
                )
                if not user:
                    self.add_error('password','La contraseña es incorrecta')
        return super(UserLoginForm,self).clean() 
