from django.core.exceptions import ValidationError


def validate_id_user(id_user):
    if not id_user.isdigit():
        raise ValidationError(('El numero de identificacion debe ser ingresado sin puntos ni espacios'),)
    elif len(id_user)<10:
        raise ValidationError(('El numero de identificacion debe ser ingresado sin puntos ni espacios y debe contener al menos 10 digitos'),)


def validate_name(name):
    if not name.isalpha():
        raise ValidationError(('El nombre debe contener solo letas A-Z'),)
    return name


def validate_last_name(last_name):
    if not last_name.isalpha():
        raise ValidationError(('El apellido debe contener solo letas A-Z'),)
    return last_name

