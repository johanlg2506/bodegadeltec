from django.db import models
#

from django.contrib.auth.models import BaseUserManager


class UserManager(BaseUserManager, models.Manager):

    def create_user(self,id_user, name, last_name,is_staff=False, is_superuser=False,  **extra_fields):
        password_vacio = ''
        user = self.model(
            id_user         = id_user       ,
            name            = name          ,
            last_name       = last_name     ,
            password        = password_vacio,
            is_staff        = is_staff      ,
            is_superuser    = is_superuser  ,
            **extra_fields
        )
        
        user.save(using=self.db)

        return user


    def create_superuser(self, id_user, name, last_name, password, is_staff=True, is_superuser=True,  **extra_fields):
        user = self.model(
            id_user         = id_user       ,
            name            = name          ,
            last_name       = last_name     ,
            is_staff        = is_staff      ,
            is_superuser    = is_superuser  ,
            **extra_fields
        )
        user.set_password(password)
        user.save(using=self.db)
        return user
        

    def filtrar_usuarios(self, kword):
        resultado = self.filter(id_user__icontains=kword,is_superuser=False)
        return resultado
    